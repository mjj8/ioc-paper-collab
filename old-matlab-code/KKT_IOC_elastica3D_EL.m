function [  ] = KKT_IOC_elastica3D_EL( )

% This file implements our proposed IOC by posing the problem as a calculus
% of variations problem and solving Euler-Lagrange equations.


options = odeset('RelTol',1e-4,'AbsTol',[1e-4 1e-4 1e-5]);
[T,Y] = ode45(@rigid,[0 12],[0 1 1],options);


plot(T,Y(:,1),'-',T,Y(:,2),'-.',T,Y(:,3),'.')


end

function dy = rigid(t,y)
dy = zeros(3,1);    % a column vector
dy(1) = y(2) * y(3);
dy(2) = -y(1) * y(3);
dy(3) = -0.51 * y(1) * y(2);

end



function dX = Euler_Lagrange(t, x)
 x(1) = c1; 
 x(2) = c2; 
 x(3) = c3; 
 x(4) = mu4 5. mu5 6. mu6 7. u1 8. u2 9. u3 10. q12 11. q22 12. q32 13. q13 14. q23 15. q33
x_dot = zeros(30, 1);
% 1. c1 2. c1 3. c3 4. mu4 5. mu5 6. mu6 7. u1 8. u2 9. u3 10. q12 11. q22 12. q32 13. q13 14. q23 15. q33
% dots (1. c1 2. c1 3. c3 4. mu4 5. mu5 6. mu6 7. u1 8. u2 9. u3 10. q12 11. q22 12. q32 13. q13 14. q23 15. q33
% Extra variables
x_dot(1) = ;

% E-L equations for mu4





% First co-state equation
2 c1 (-c2 u2[t] u3[t]+c3 u2[t] u3[t]+c1 u1[t]^\[Prime]) u1[t]^\[Prime]\[Prime]-2 c1 (c1 u1[t]^\[Prime]\[Prime] (u1^\[Prime])[t]-c2 u3[t] (u2^\[Prime])[t]+c3 u3[t] (u2^\[Prime])[t]-c2 u2[t] (u3^\[Prime])[t]+c3 u2[t] (u3^\[Prime])[t])


2 (-c2 u3[t]+c3 u3[t]) (-c2 u2[t] u3[t]+c3 u2[t] u3[t]+c1 u1[t]^\[Prime])


2 (-c2 u2[t]+c3 u2[t]) (-c2 u2[t] u3[t]+c3 u2[t] u3[t]+c1 u1[t]^\[Prime])


2 u1[t]^\[Prime] (-c2 u2[t] u3[t]+c3 u2[t] u3[t]+c1 u1[t]^\[Prime])


-2 u2[t] u3[t] (-c2 u2[t] u3[t]+c3 u2[t] u3[t]+c1 u1[t]^\[Prime])


2 u2[t] u3[t] (-c2 u2[t] u3[t]+c3 u2[t] u3[t]+c1 u1[t]^\[Prime])




end


function [xopt, uopt, popt, xoptp, uoptp, poptp] = elasticaacadoMain(t0, tf, x0, xf, c, Mx, Mu)

out = acado_elastica_RUN(c(1),c(2),c(3),c(4),c(5),xf(1),xf(2),xf(3), Mx, Mu);

topt = out.STATES(:,1);
xoptsamp = out.STATES(:,2:end);
uoptsamp = out.CONTROLS(:,2:end);

% interpolate x, and u to get obtain state and input as functions of time
xtmp = csaps(topt, xoptsamp', 1);
utmp = csaps(topt, uoptsamp', 1);
xopt = @(t) fnval(xtmp,t);
uopt = @(t) fnval(utmp,t);

xtmpprime = fnder(xtmp);
utmpprime = fnder(utmp);
xoptp = @(t) fnval(xtmpprime,t);
uoptp = @(t) fnval(utmpprime,t);

popt = nan;
poptp = nan;

end


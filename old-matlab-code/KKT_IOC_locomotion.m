function [ out ] = KKT_IOC_locomotion( t0, tf, x0list, xflist, c, xoptlist, uoptlist, Voptlist, timestamp)

global n m nn M k tf

k = length(c)
n = length(x0list{1})
m = length(uoptlist{1}(0))
M = length(xoptlist)
nn = k + M*n


for i=1:M
    costopt{i} = c'*Voptlist{i};
end

A = zeros(nn,nn);
B = zeros(nn, M*n);
B(k+1:end,:) = eye(M*n);

% Riccati solution
tic;
%K = computeK()
Pf = zeros(nn*nn,1);
%Pf = reshape(K'*K, nn*nn,1);
options = odeset('RelTol',1e-8,'AbsTol',1e-8);
sol = ode45(@(t,x)rde(t, x, A, B, xoptlist, uoptlist, xflist), [tf, t0], Pf, options);
P = @(t)deval(sol,t);

% Minimize P(0) w.r.t. z(0) to find cost params and initial costates
P0 = reshape(P(0),nn,nn)
Aineq = zeros(k,nn); 
Aineq(1:k,1:k) = -eye(k);
bineq = zeros(k,1);
bineq(2:4) = -0.01;
Aeq = zeros(1,nn);
Aeq(1) = 1;
beq = 1.2;
options = optimset('Algorithm','interior-point-convex','TolFun',1e-14,'TolX',1e-14,'TolCon',1e-14);
[zhat, fval, exitflag, qpoutput, qplambda] = quadprog(P0, [], Aineq, bineq, Aeq, beq,[],[],[],options);
totalTime = toc;


out.totalTime = totalTime;
out.t0 = t0;
out.tf = tf;
out.x0 = x0list;
out.xf = xflist;
out.c = c;
out.costopt = costopt;
out.xopt = xoptlist;
out.uopt = uoptlist;
out.Vopt = Voptlist;
out.P0 = P0;
out.zhat = zhat;
out.fval = fval;
out.chat = zhat(1:k);
out.p0hat = zhat(k+1:end);
out.exitflag = exitflag;
out.qpoutput = qpoutput;
out.qplambda = qplambda;

outdir = userpath;
outdir = outdir(1:end-1);
if timestamp,
    filename = [outdir filesep timestamp '-kkt-locomotion.mat'];
else
    filename = [outdir filesep datestr(now,'yyyymmdd-HHMMSS') '-kkt-locomotion.mat'];
end

save(filename, 'out');

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Helper functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Pdot = rde(t, x, A, B, xoptlist, uoptlist, xflist)
% Riccati Differential Equation
global nn tf

t
if t > tf
    P = zeros(nn,nn);
    Pdot = zeros(nn*nn,1);
    return
else
    P = reshape(x,nn,nn);
end
Qt = Q(t, xoptlist, uoptlist, xflist);
Rt = R();
St = S(t, xoptlist, uoptlist, xflist);

Pdot = -A'*P - P*A + (P*B + St)*inv(Rt)*(P*B + St)' - Qt;
Pdot = reshape(Pdot, nn*nn, 1);

end

function fx = dfdx(t, xt, ut)
global n
fx = zeros(n,n);
fx(1,3) = -xt(4)*sin(xt(3)) - xt(6)*cos(xt(3));
fx(1,4) = cos(xt(3));
fx(1,6) = -sin(xt(3));
fx(2,3) = xt(4)*cos(xt(3)) - xt(6)*sin(xt(3));
fx(2,4) = sin(xt(3));
fx(2,6) = cos(xt(3));
fx(3,5) = 1;
end

function fu = dfdu(t, xt, ut)
global n m
fu = zeros(n,m);
fu(4,1) = 1;
fu(5,2) = 1;
fu(6,3) = 1;
end

function phix = dphidx(t, xt, ut, xf)
global k n
phix = zeros(k,n);
if norm(xf(1:2)-xt(1:2)) > 0
phix(4,1) = - ( 2*(xf(2)-xt(2))*(angle_diff(atan2(xf(2) - xt(2), xf(1) - xt(1)) - xt(3), 0))) ...
    /(norm(xf(1:2)-xt(1:2))^2);
phix(4,2) = - ( 2*(xf(1)-xt(1))*(angle_diff(atan2(xf(2) - xt(2), xf(1) - xt(1)) - xt(3), 0))) ...
    /(norm(xf(1:2)-xt(1:2))^2);
end
phix(4,3) = -2*(angle_diff(atan2(xf(2) - xt(2), xf(1) - xt(1)) - xt(3), 0));
end

function phiu = dphidu(t, xt, ut)
global k n m
phiu = zeros(k,m);
phiu(1,1) = 2*ut(1);
phiu(2,2) = 2*ut(2);
phiu(3,3) = 2*ut(3);
end

function xix = dxidx()
global n
xix = zeros(1,n);
xix(end) = 1;
end

function Abar = computeAbar(t, xt, ut, xf)
global n m k
Abar = zeros(n+m,k);
Abar(1:n,:) = dphidx(t,xt,ut,xf)';
Abar(n+1:n+m,:) = dphidu(t,xt,ut)';
end

function Bbar = computeBbar(t, xt, ut)
global n m
Bbar = zeros(n+m,n);
Bbar(1:n,:) = dfdx(t, xt, ut)';
Bbar(n+1:n+m,:) = dfdu(t, xt, ut)';
end

function Cbar = computeCbar()
global n m
Cbar = zeros(n+m,n);
Cbar(1:n,:) = eye(n);
end

function F = computeF(t, xoptlist, uoptlist, xflist)
global n m k M
F = zeros(M*(n+m), k + M*n);
for i = 1:M
    xt = xoptlist{i}(t);
    ut = uoptlist{i}(t);
    xf = xflist{i};
    i1 = (i-1)*(n+m)+1;   % row start index
    i2 = (i)*(n+m);       % row end   index
    j1 = k + 1 + (i-1)*n; % column start index
    j2 = k + 1 + (i)*n - 1;   % column end   index
    F(i1:i2, 1:k) = computeAbar(t, xt, ut, xf);
    F(i1:i2, j1:j2) = computeBbar(t, xt, ut);
end
end

function G = computeG()
global n m M
G = zeros(M*(n+m), M*n);
for i = 1:M
    i1 = (i-1)*(n+m)+1;  % row start index
    i2 = (i) * (n+m);    % row   end index
    G(i1:i2, :) = computeCbar();
end
end

function K = computeK()
global n k M
K = zeros(M*n, k+M*n);
for i = 1:M
    i1 = (i-1)*(n)+1;   % row start index
    i2 = (i)*(n);       % row end   index
    j1 = k + 1 + (i-1)*n; % column start index
    j2 = k + 1 + (i)*n - 1;   % column end   index
    K(i1:i2,k) = dxidx()';
    K(i1:i2,j1:j2) = -eye(n);
end
end

function Qout = Q(t, xoptlist, uoptlist, xflist)
Ft = computeF(t, xoptlist, uoptlist, xflist);
Qout = Ft'*Ft;
end

function Rout = R()
Gt = computeG();
Rout = Gt'*Gt;
end

function Sout = S(t, xoptlist, uoptlist, xflist)
Ft = computeF(t, xoptlist, uoptlist, xflist);
Gt = computeG();
Sout = Ft'*Gt;
end


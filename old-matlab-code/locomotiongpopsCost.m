function [ terminalCost, integrandCost, terminalJac, integrandJac ] = locomotiongpopsCost( sol )

global c xf

N = length(sol.time);
t = sol.time;    % N x 1 column vector
x = sol.state;   % N x n matrix, N number of collocation points, n is dimension of state
u = sol.control; % N x m matrix, N number of collocation points, m is dimension of control
p = sol.parameter;

n = length(x(1,:));
m = length(u(1,:));

terminalCost = 0; % terminal cost

integrandCost = zeros(size(t));
for i = 1:N
    integrandCost(i) = c' * phi_locomotion(t(i),x(i,:)',u(i,:)',xf);
end

terminalJac = [zeros(1,n), 0, zeros(1,n), 0, zeros(1,length(p))];

dL = zeros(N,n+m);
for i=1:N
    %dL(i,:) = 2*[c(4)*(xf(2)-x(i,2))/((xf(1)-x(i,1))^2 + (xf(2)-x(i,2))^2), ...
    %             c(4)*(xf(1)-x(i,1))/((xf(1)-x(i,1))^2 + (xf(2)-x(i,2))^2), ...
    %             c(4)*(-1), 0,0,0, ...
    %             c(1)*u(i,1), c(2)*u(i,2), c(3)*u(i,3)];
    dL(i,:) = 2*[0,0,0,0,0,0,c(1)*u(i,1), c(2)*u(i,2), c(3)*u(i,3)];
end
integrandJac = [ dL, zeros(size(t)), zeros(length(t),length(p))];



end


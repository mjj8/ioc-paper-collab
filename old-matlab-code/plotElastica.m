function out = plotElastica(x1, x2)

ts = linspace(0,1,100);
x1s = x1(ts);
x2s = x2(ts);
%x3s = x3(ts);

h = figure;
plot(x1s(1,:),x1s(2,:),'b-');
hold on;
plot(x2s(1,:),x2s(2,:),'g-');
axis equal;
%plot(x3s(1,:),x3s(2,:),'g-');

end
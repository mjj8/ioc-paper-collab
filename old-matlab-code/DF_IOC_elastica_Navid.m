function [percentage_error, sum_squared_error] = DF_IOC_elastica_Navid( t0, tf, x0list, xflist, c, xoptlist, uoptlist, Voptlist, costoptlist, tguess, xguess, uguess, timestamp)

global k;
k = length(c);



t_range = linspace(t0, tf, 100);
% ut = uoptlist{1}(t_range);
ut = uoptlist(t_range);

phidu = dphidu(t_range, [], ut);

y_vec =  -0.1* phidu(1,:);
A_mat = phidu(2:end,:);

AT_A = [];
for i = 1:length(A_mat)
    AT_A = [AT_A, reshape(A_mat(:, i)*A_mat(:,i)', (k-1)^2, 1)];
end

AT_A_int = [];
for i = 1: (k-1)^2
    AT_A_int = [AT_A_int; trapz(t_range, AT_A(i, :))];
end
AT_A_int = reshape(AT_A_int, k-1, k-1);
AT_A_int_inv = inv(AT_A_int);


AT_y = [];
for i = 1:length(A_mat)
    AT_y = [AT_y, A_mat(:, i)*y_vec(i)];
end

AT_y_int = [];
for i = 1:(k-1)
    AT_y_int = [AT_y_int; trapz(t_range, AT_y(i, :))];
end


c
% chat = AT_A_int_inv * AT_y_int;
chat = AT_A_int \ AT_y_int;

chat = [0.1; chat]

% chat = inv(trapz(t_range, A_mat*A_mat')) * (trapz(t_range, A_mat * (-0.2 * y_vec)))

percentage_error = norm(c - chat)/norm(c) * 100
sum_squared_error = norm(c - chat)


end

    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Helper functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function phix = dphidx(t, xt, ut)
global k n
phix = zeros(k,n);
end

function phiu = dphidu(t, xt, ut)
global k
% phiu = sigma(t,k) * 2 * ut;
phiu = sigma(t,k) * 2 .* (ones(k, 1)*ut);
end

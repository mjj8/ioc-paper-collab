function [ output ] = maxmargin_elastica2_objective( c, t0, tf, x0, xf, Vopt, k )
%output = maxmargin_elastica2_objective( c )
%
%   c: variables of optimization = weights in forward problem cost function
%

c

lambda = 1;

%Vopt = FeatureExpectations(xopt, uopt, t0, tf, @phi_elastica, k);
cost_opt = c' * Vopt;

% min c' * FeatureExpectation(mu)   (or loss-function variant)
[x, u, ~, ~, ~, ~] = elasticagpopsMain(t0, tf, x0, xf, c);
V = FeatureExpectations(x,u,t0,tf,@phi_elastica,k);
cost = c' * V;

output = cost_opt - cost + (lambda/2)*norm(c)^2

end


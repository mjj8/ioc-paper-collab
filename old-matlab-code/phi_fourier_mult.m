function [ phi, dphi ] = phi_fourier_mult( t, x, u, k )
%
% t: N x 1
% x: n x N 
% u: m x N
% 
% k: order of fourier basis   e.g.  cos(2 pi i x) , where i = 0, ..., k
% Note: number of basis funcs = 1 + 2*k*(n+m)
%


tsamp = t;

if strcmp(class(x),'function_handle')
    xsamp = x(tsamp);
else
    xsamp = x;
end
if strcmp(class(u),'function_handle')
    usamp = u(tsamp);
else
    usamp = u;
end

n = size(xsamp,1);
m = size(usamp,1);

kk = 1 + 2*k*(n+m);
N = length(tsamp);
phi = zeros(kk,N);
dphi = zeros(kk,n+m,N);

for i=1:N
    phi(1,i) = 1;
    counter = 2;
    for j = 1:k
        for jj = 1:n
            phi(counter,i) = 1 + cos(2*pi*j*xsamp(jj,i));   %sigma(t(i), k) * usamp(i)^2;
            counter = counter + 1;
        end
        for jj = 1:n
            phi(counter,i) = 1 + sin(2*pi*j*xsamp(jj,i));
            counter = counter + 1;
        end
    end
    for j = 1:k
        for jj = 1:m
            phi(counter,i) = 1 + cos(2*pi*j*usamp(jj,i));
            counter = counter + 1;
        end
        for jj = 1:m
            phi(counter,i) = 1 + sin(2*pi*j*usamp(jj,i));
            counter = counter + 1;
        end
    end
end

%% Compute jacobian
for i=1:N
   dphi(1,:,i) = zeros(1,n+m);
   counter = 2;
   for j = 1:k
       for jj = 1:n
           dphi(counter,jj,i) = -2*pi*j*sin(2*pi*j*xsamp(jj,i));
           counter = counter + 1;
       end
   %end
   %for j = 1:k
       for jj = 1:n
           dphi(counter,jj,i) = 2*pi*j*cos(2*pi*j*xsamp(jj,i));
           counter = counter + 1;
       end
   end
   for j = 1:k
        for jj = 1:m
            dphi(counter,n+jj,i) = -2*pi*j*sin(2*pi*j*usamp(jj,i));
            counter = counter + 1;
        end
        for jj = 1:m
            dphi(counter,n+jj,i) = 2*pi*j*cos(2*pi*j*usamp(jj,i));
            counter = counter + 1;
        end
    end
end

end


function [x, u, p] = getElasticaGivenP0(x0, p0, c)


%%%%%%%%%% set up global variables %%%%%%%%%
%global p0 k2 c2
%
%p0 = [20 -20 6]'; % initial costate
%k2 = 1 + 2*4;  % number of basis functions in cost
%c2 = [1 0 0 0 0 0 0 0 0]';
%%%%%%%%%% end global variables %%%%%%%%%

n = 3;
m = 1;
k = length(c);
t0 = 0;
tf = 1.0;

options = odeset('RelTol',1e-10,'AbsTol',1e-10);
sol = ode45(@(t,x)elasticaEOM(t, x, @(t,x)elasticaMuCostate0(t,x,p0,c)), [t0 tf], x0, options);

x = @(t) deval(sol,t);
% Navid
x([0.5, 0.75])
u = @(t) elasticaMuCostate0(t,x(t),p0,c);
%p = @(t) [p0(1); p0(2); x(t)'.*[-p0(2), p0(1), 0]' + p0(3)];
p = @(t) vertcat(repmat([p0(1);p0(2)],1,length(t)), (x(t)'*[-p0(2), p0(1), 0]')' + p0(3));


%t = linspace(0,1,100)';
%N = length(t);
%xhist = zeros(N,n);
%uhist = zeros(N,m);
%for i = 1:N
%    xhist(i,:) = deval(sol,t(i));
%    uhist(i) = elasticaMuCostate0(t(i),xhist(i,:)',p0,c);
%end

    
end
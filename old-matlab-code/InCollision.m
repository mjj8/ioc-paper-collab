function [res,B1,B2] = InCollision(conf,tol,w,s)

B = GetBall(conf,0,1,w);
Bs = GetStartCap(conf,w,s);
Bg = GetGoalCap(conf,w,s);
% Each row of Q is a pair of balls
Q = {B,B,2; Bs,B,1; B,Bg,1; Bs,Bg,0};
tmin = 1;
while (~isempty(Q))
    
    % Remove a pair of balls from the front of the Q
    B1 = Q{1,1};
    B2 = Q{1,2};
    mark = Q{1,3};
    Q = Q(2:end,:);
    
    % Check the marker
    if (mark == 2) % B1 and B2 are the same
        if (B1.dt>tol) % B1 is not a lowest-level ball
            [BL,BR] = DivideBall(conf,B1);
            Q(end+1,:) = {BL,BL,2};
            Q(end+1,:) = {BL,BR,1};
            Q(end+1,:) = {BR,BR,2};
        end
    elseif (mark == 1) % B1 and B2 are adjacent
        if ((B1.dt<=tol)&&(B2.dt<=tol)) % both B1 and B2 are lowest-level
            % Do nothing!
        elseif (B1.dt>B2.dt)
            [BL,BR] = DivideBall(conf,B1);
            Q(end+1,:) = {BL,B2,0};
            Q(end+1,:) = {BR,B2,1};
        else
            [BL,BR] = DivideBall(conf,B2);
            Q(end+1,:) = {B1,BL,1};
            Q(end+1,:) = {B1,BR,0};
        end
    else % B1 and B2 are neither the same nor adjacent
        if (InCollision_TwoBalls(B1,B2))
            if ((B1.dt<=tol)&&(B2.dt<=tol)) % both B1 and B2 are lowest-level
                if (GetDistance(B1.p1,B1.p2,B2.p1,B2.p2)<2*w)
                    res=1;
                    return;
                end
            elseif (B1.dt>B2.dt)
                [BL,BR] = DivideBall(conf,B1);
                Q(end+1,:) = {BL,B2,0};
                Q(end+1,:) = {BR,B2,0};
            else
                [BL,BR] = DivideBall(conf,B2);
                Q(end+1,:) = {B1,BL,0};
                Q(end+1,:) = {B1,BR,0};
            end
        end
    end
    
end
res=0;
B1=[];
B2=[];


function res = InCollision_TwoBalls(B1,B2)
d = norm(B2.x-B1.x);
res = d<(B1.r+B2.r);

function [B1,B2] = DivideBall(conf,B)
tmid = (B.t1+B.t2)/2;
pmid = GetPoint(conf,tmid);
% First Ball
B1 = B;
B1.t2 = tmid;
B1.p2 = pmid;
B1.x = (B1.p1+B1.p2)/2;
B1.dt = B1.t2-B1.t1;
B1.r = (B1.dt/2)+B1.w;
% Second Ball
B2 = B;
B2.t1 = tmid;
B2.p1 = pmid;
B2.x = (B2.p1+B2.p2)/2;
B2.dt = B2.t2-B2.t1;
B2.r = (B2.dt/2)+B2.w;

function B = GetBall(conf,t1,t2,w)
B.t1 = t1;
B.t2 = t2;
B.dt = B.t2-B.t1;
B.w = w;
B.p1 = GetPoint(conf,B.t1);
B.p2 = GetPoint(conf,B.t2);
B.x = (B.p1+B.p2)/2;
B.r = (B.dt/2)+B.w;

function B = GetStartCap(conf,w,s)
B.t1 = [];
B.t2 = [];
B.dt = 0;
B.w = w;
B.p1 = [-s;0;0];
B.p2 = [0;0;0];
B.x = (B.p1+B.p2)/2;
B.r = (s/2)+B.w;

function B = GetGoalCap(conf,w,s)
B.t1 = [];
B.t2 = [];
B.dt = 0;
B.w = w;
p = [0 0 0 1; s 0 0 1]';
T = reshape(deval(conf.qsol,1),4,4);
p = T*p;
B.p1 = p(1:3,1);
B.p2 = p(1:3,2);
B.x = (B.p1+B.p2)/2;
B.r = (s/2)+B.w;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% HERE ARE THE TWO FUNCTIONS YOU WOULD CHANGE
% IF YOU WANTED TO MOVE FROM 3D TO 2D

function d = GetDistance(a1,a2,b1,b2)
d = DistBetween2Segment(a1',a2',b1',b2');

function p = GetPoint(conf,t)
q = deval(conf.qsol,t);
p = q(13:15);

%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%






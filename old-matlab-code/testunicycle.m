
%%% a few simple tests for unicyclegpopsMain:

if 0,
    t0 = 0; % initial time
    tf = 5; % final time
    x0 = [2,2,0]'; % initial state
    c = [10,10,1,1,1];  % Cost function parms. Note:  Q = diag(c(1:3)), R = diag(c(4:5))
end

if 1,
    t0 = 0; % initial time
    tf = 5; % final time
    x0 = [2,2,0]'; % initial state
    c = [5,5,1,1,1];  % Cost function parms. Note:  Q = diag(c(1:3)), R = diag(c(4:5))  
end
    
% solve forward optimal control problem
[xopt, uopt, popt, xoptp, uoptp, poptp] = unicyclegpopsMain(t0, tf, x0, c);
plotUnicycle(0,5,xopt); % plot x trajectory
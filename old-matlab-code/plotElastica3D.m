function out = plotElastica3D(x,u)

ts = linspace(0,1,100);

xs = x(ts);
us = u(ts);

reshape(x(0),4,4)
reshape(x(1),4,4)

h = figure;
plot(us(1,:),'b-');
hold on;
plot(us(2,:),'g-');
plot(us(3,:),'r-');
%plot(x1s(1,:),x1s(2,:),'b-');
%plot(x2s(1,:),x2s(2,:),'g-');
%axis equal;
%plot(x3s(1,:),x3s(2,:),'g-');
xlabel('t');
ylabel('u');
end
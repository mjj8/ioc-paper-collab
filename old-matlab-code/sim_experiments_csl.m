clear global variables

global c xf;
    
curFolder = pwd;
addpath(curFolder);

%% LQR %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if 0,
    timestamp = datestr(now,'yyyymmdd-HHMMSS');
    t0 = 0;
    tf = 7;
    x0 = [2, 2, 0]';
    xf = nan;
    c = [1.0, 2.0, 0.05, 0.1, 0.2]';
    %out = Direct_IOC_lqr(t0, tf, x0, xf, c, timestamp);
    %out1 = MaxMargin_IOC_lqr(t0, tf, x0, xf, c, timestamp);
    %out2 = MaxMargin_IOC_lqr2(t0, tf, x0, xf, c, timestamp);
    %out3 = MaxMargin_IOC_lqr3(t0, tf, x0, xf, c, timestamp);
end

%% Unicycle %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if 0,
    timestamp = datestr(now,'yyyymmdd-HHMMSS');
    t0 = 0;
    tf = 7;
    x0 = [2, 2, 0]';
    xf = nan;
    c = [1.0, 2.0, 0.05, 0.1, 0.2]';
    %out = Direct_IOC_unicycle(t0, tf, x0, xf, c, timestamp);
    out1 = MaxMargin_IOC_unicycle(t0, tf, x0, xf, c, timestamp);
    %out2 = MaxMargin_IOC_unicycle2(t0, tf, x0, xf, c, timestamp);
    %out3 = MaxMargin_IOC_unicycle3(t0, tf, x0, xf, c, timestamp);
end


%% Locomotion %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if 0,
    t0 = 0;
    tf = nan;
    x0 = [0,0,pi/2,0,0,0]';
    if 1,
        timestamp = datestr(now,'yyyymmdd-HHMMSS');
        %xf = [-2, 1, 3*pi/4, 0, 0, 0]';  % Mombaur's target 2
        xf = [3,1,pi/4,0,0,0]';  % Mombaur's target 1
        %xf = [3, -1, -pi/4, 0, 0, 0]';
        c = [1, 1.2, 1.7, 0.7, 5.2]';
        %c = [1, 0.01, 0.01, 0.01, 0]';
        tguess = [t0; 5];
        xguess = [x0'; xf'];
        uguess = [[0,0,0]; [0,0,0]];
        tic
        [xopt, uopt, ~, ~, ~, ~,tfopt] = locomotiongpopsMain(t0, tf, x0, xf, c, tguess, xguess, uguess);
        toc
%         tguess = linspace(t0,tf,50)';
%         xguess = xopt(tguess)';
%         uguess = uopt(tguess)';
%         tf = nan;
%         [xopt2, uopt2, ~, ~, ~, ~,tfopt2] = locomotiongpopsMain(t0, tf, x0, xf, c, tguess, xguess, uguess);
        %tguess = linspace(0,tfopt,50)';
        %%xguess = zeros(length(tguess), 7);
        %%xguess(:,2) = linspace(0,xf(1),50)';
        %%ugues = zeros(length(tguess), 3);
        %xguess = xopt(tguess)';
        %uguess = uopt(tguess)';
        %Mx = [tguess, xguess, zeros(length(tguess),1)];
        %Mu = [tguess, uguess];
        %tic
        %[x1, u1, ~, ~, ~, ~, tfopt2] = locomotionacadoMain(t0, tf, x0, xf, c, Mx, Mu);
        %toc
        %out = Direct_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        out1 = MaxMargin_IOC_locomotion(t0, tf, x0, xf, c, timestamp);
        %out1a = MaxMargin_IOC_elastica_a(t0, tf, x0, xf, c, timestamp);
        %out2 = MaxMargin_IOC_elastica2(t0, tf, x0, xf, c, timestamp);
        %out3 = MaxMargin_IOC_elastica3(t0, tf, x0, xf, c, timestamp);
    end
end


%% Elastica %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if 1,
    t0 = 0;
    tf = 1;
    x0 = [0,0,0]';
    if 0,
        timestamp = datestr(now,'yyyymmdd-HHMMSS');
        xf = [0.1687, -0.6698, 0.4209]';
        c = [1, 0, 0, 0, 0]';
        %out = Direct_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        out1 = MaxMargin_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        out1a = MaxMargin_IOC_elastica_a(t0, tf, x0, xf, c, timestamp);
        %out2 = MaxMargin_IOC_elastica2(t0, tf, x0, xf, c, timestamp);
        %out3 = MaxMargin_IOC_elastica3(t0, tf, x0, xf, c, timestamp);
    end
    if 0,
        timestamp = datestr(now,'yyyymmdd-HHMMSS');
        xf = [0.1687, -0.6698, 0.4209]';
        c = [1, 2, 0, 2, 0]';
        %out = Direct_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        %out1 = MaxMargin_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        out1a = MaxMargin_IOC_elastica_a(t0, tf, x0, xf, c, timestamp);
        %out2 = MaxMargin_IOC_elastica2(t0, tf, x0, xf, c, timestamp);
        %out3 = MaxMargin_IOC_elastica3(t0, tf, x0, xf, c, timestamp);
    end
    if 0,
        timestamp = datestr(now,'yyyymmdd-HHMMSS');
        xf = [-0.0555, -0.1816, 1.3209]';
        c = [1, 2, 0, 2, 0]';
        out = Direct_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        out1 = MaxMargin_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        %out2 = MaxMargin_IOC_elastica2(t0, tf, x0, xf, c, timestamp);
        out3 = MaxMargin_IOC_elastica3(t0, tf, x0, xf, c, timestamp);
    end
    if 0,
        timestamp = datestr(now,'yyyymmdd-HHMMSS');
        xf = [0.9558, -0.2295, 0.1126]';
        c = [1, 3, 1.2, 0.5, 0]';
        out = Direct_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        out1 = MaxMargin_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        %out2 = MaxMargin_IOC_elastica2(t0, tf, x0, xf, c, timestamp);
        out3 = MaxMargin_IOC_elastica3(t0, tf, x0, xf, c, timestamp);
    end
    if 1,
        seed = 1;
        rngstate = rng(seed,'twister');  % initialize random number generator
        
        timestamp = datestr(now,'yyyymmdd-HHMMSS');
        p0 = [16,7,17]';
        c = [1; 3*rand(4,1)];
        k = length(c);
        [xopt, uopt, popt] = getElasticaGivenP0(x0, p0, c);
        Vopt = FeatureExpectations(xopt, uopt, t0, tf, @phi_elastica, k);
        xf = xopt(1);
        xf(3) = angle_diff(xf(3),0);
        out = Direct_IOC_elastica(t0, tf, x0, xf, c, xopt, uopt, Vopt, timestamp);
        %out1 = MaxMargin_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        %out3 = MaxMargin_IOC_elastica3(t0, tf, x0, xf, c, timestamp);
    end
end
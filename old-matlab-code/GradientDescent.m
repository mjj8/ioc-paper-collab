function [conf,alwaysfree] = GradientDescent(conf,Rgoal,pgoal,maxerr,k,D)
eta = ones(6,1);
maxiter = 1000;
iter = 0;
while norm(eta)>maxerr
    iter = iter+1;
    if (iter>maxiter)
        alwaysfree = 0;
        return
    end
    
%     % Find current position and orientation
%     J = reshape(conf.J(:,end),6,6);
%     T = reshape(conf.q(:,end),4,4);
%     R = T(1:3,1:3);
%     p = T(1:3,4);
%     
%     % Find error in position and orientation, in body frame, using Euler
%     % angles for orientation
%     Rerr = Rgoal*(R');
%     hXYZ = GetXYZ(Rerr);
%     bXYZ = GetB_XYZ(hXYZ);
%     perr = R'*(pgoal-p);
%     eta = [hXYZ;perr];
%     e = norm(eta);
%     
%     % Find descent direction
%     DA = ([hXYZ'*bXYZ perr']*J)'
%     
%     % Do backtracking line search
%     alpha = 0.1;
%     beta = 0.25;
%     t = 1;
%     while (1)
%         fprintf(1,'   line: t=%10.4f\n',t);
%         confcur = GetConfiguration(conf.a+t*DA);
%         ecur = GetError(confcur,Rgoal,pgoal);
%         if (ecur <= e-alpha*t*(DA'*DA))
%             break;
%         else
%             t = beta*t;
%         end
%     end
%     conf = confcur;
    
    % Find current position and orientation
    J = reshape(conf.J(:,end),6,6);
    T = reshape(conf.q(:,end),4,4);
    R = T(1:3,1:3);
    p = T(1:3,4);
    
    % Find desired change in position and orientation
    dh = GetXYZ(R'*Rgoal);
    dp = R'*(pgoal-p);
    eta = [dh;dp];
    
    % Find change "da" in "a" that would induce this desired change in
    % position and orientation, valid for small "da"
    da = J\eta;
    
    % Do backtracking line search
    alpha = 0.25;
    beta = 0.75;
    t = 1;
    e = norm(eta);
    while (1)
        fprintf(1,'   line: t=%10.4f\n',t);
        
        
        confcur = GetConfiguration(conf.a+t*da);
        
        ecur = GetError(confcur,Rgoal,pgoal);
        dJ = J*da;
        elinear = norm(eta-t*dJ);
        
%         e-ecur
%         alpha*(e-elinear)
%         
%         error('stop')
        
        
        
        if ((e-ecur) > alpha*(e-elinear))
            break;
        else
            t = beta*t;
        end
    end
    conf = confcur;
    
    
    
    
%     % Take a small step along "da"
%     conf = GetConfiguration(conf.a+k*da);
    

%     newconf = GetConfiguration(a+da);
% 
%     J = reshape(newconf.J(:,end),6,6);
%     T = reshape(newconf.q(:,end),4,4);
%     Rnew = T(1:3,1:3);
%     pnew = T(1:3,4);
% 
%     % [ang,axe] = GetExpCoords(Rnew*(R'))
%     % ang*axe
%     % h = GetXYZ(Rnew*(R'))
%     dh = GetXYZ(R'*Rnew);
%     dp = R'*(pnew-p);
%     deta = [dh;dp];
%     [eta deta]
    
    
    
    
    
    
    
    
% %     error('stop');
% %     
% %     
% %     
% %     Rerr = Rgoal*(R');
% %     
% %     % when small, these angles are like eta(1:3)
% %     hXYZ = GetXYZ(Rerr)
% %     
% % %     [ang,axe] = GetExpCoords(Rerr)
% % %     (1/(2*sin(ang)))
% % %     norm(axe)
% %     
% %     perr = R'*(pgoal-p);
% %     
% % %     eta = [ang*axe; perr];
% %     eta = [hXYZ; perr]
% %     
% %     DA = J\eta
%     conf = GetConfiguration(conf.a+k*DA);
    if (conf.infeasible)
        alwaysfree = 0;
        return;
    end
    if (nargin==6)
        D = UpdateDisplay(conf,D);
    end
    fprintf(1,'  error: %12.8f\n',norm(eta));
end
fprintf(1,'  error: %12.8f\n',norm(eta));
alwaysfree = 1;



% function etacoords = InverseWedge(eta,X)
% for i=1:3
%     etacoords(i,1) = sum(sum(X{i}.*eta))/2;
% end
% for i=4:6
%     etacoords(i,1) = sum(sum(X{i}.*eta));
% end




return

function e = GetError(conf,Rgoal,pgoal)
J = reshape(conf.J(:,end),6,6);
T = reshape(conf.q(:,end),4,4);
R = T(1:3,1:3);
p = T(1:3,4);
dh = GetXYZ(R'*Rgoal);
dp = R'*(pgoal-p);
eta = [dh;dp];
e = norm(eta);

% J = reshape(conf.J(:,end),6,6);
% T = reshape(conf.q(:,end),4,4);
% R = T(1:3,1:3);
% p = T(1:3,4);
% Rerr = Rgoal*(R');
% hXYZ = GetXYZ(Rerr);
% bXYZ = GetB_XYZ(hXYZ);
% perr = R'*(pgoal-p);
% eta = [hXYZ;perr];
% e = norm(eta);








return;




% 
% 
% bstep = 1e-1;
% ns = 1+ceil(Bdist/bstep)
% s = linspace(0,1,ns);
% bstart = Estart.x(:,end);
% bgoal = Egoal.x(:,end);
% db = [bgoal(1:2)-bstart(1:2); AngDiff(bgoal(3),bstart(3))];
% k = 1e-1;
% maxerr = 1e-4;
% E = Estart;
% for i=1:ns
%     
%     b = bstart + s(i)*db;
%     [E,alwaysfree] = GradientDescent(E,b,maxerr,k);
%     G.startrobB = UpdateRobot(G.startrobB,E.x,1);
%     drawnow;
%     
%     if (~E.free)
%         fprintf(1,'B - infeasible\n');
%         break;
%     end
%     
%     if (~alwaysfree)
%         fprintf(1,'B - infeasible in gradient descent\n');
%         break;
%     end
%     
% end
% [E.a,Egoal.a]
% if (norm(E.a-Egoal.a)>100*maxerr)
%     fprintf(1,'B - infeasible connection (ended at wrong A)\n');
% end

function B = GetB_XYZ(h)
B = [cos(h(3))*sec(h(2)) -sin(h(3))*sec(h(2)) 0
     sin(h(3)) cos(h(3)) 0
     -cos(h(3))*tan(h(2)) sin(h(3))*tan(h(2)) 1];

function hXYZ = GetXYZ(Rerr)
h2 = asin(Rerr(1,3));
h1 = atan2(-Rerr(2,3),Rerr(3,3));
h3 = atan2(-Rerr(1,2),Rerr(1,1));
hXYZ = [h1;h2;h3];

function [mu,a] = GetExpCoords(R)
mu = acos((trace(R)-1)/2);
a = (1/(2*sin(mu)))*[R(3,2)-R(2,3); R(1,3)-R(3,1); R(2,1)-R(1,2)];


function R=GetR(mu,a)
R = eye(3)+(wedge(a)*sin(mu))+((wedge(a)^2)*(1-cos(mu)));

function S=wedge(a)
S = [0 -a(3) a(2); a(3) 0 -a(1); -a(2) a(1) 0];
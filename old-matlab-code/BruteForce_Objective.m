

function Objective_value = BruteForce_Objective(c_hat_remainder, c_hat_first, c_opt, t0, tf, x0, xf, x_observed)

global iter;
%c_opt = [1 -2 3 0 0 0 0 0 0]';
%c_opt = c;
% c_opt = 13;

%x_observed = Forward_Problem(t0, tf, x0, xf,c_opt);
c_hat = [c_hat_first; c_hat_remainder];
x_predicted = Forward_Problem(t0, tf, x0, xf,c_hat);

%disp('objective_value = ');
Objective_value = sum((x_observed - x_predicted).^2)

blah = [];

drawnow, figure(1), semilogy(iter, Objective_value, 'o-')
drawnow, figure(2), semilogy(iter, norm(c_hat - c_opt), 'o-')

iter = iter + 1;
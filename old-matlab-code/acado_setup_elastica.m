clear;

BEGIN_ACADO;

    %% Initialize ACADO parameters
    acadoSet('problemname','acado_elastica');
    
    DifferentialState x1 x2 x3;
    Control u;
    
    TIME t;
    
    c1 = acado.MexInput;
    c2 = acado.MexInput;
    c3 = acado.MexInput;
    c4 = acado.MexInput;
    c5 = acado.MexInput;
    
    xf1 = acado.MexInput;
    xf2 = acado.MexInput;
    xf3 = acado.MexInput;
    
    Mx = acado.MexInputMatrix;
    Mu = acado.MexInputMatrix;

    %% Equations of motion
    f = acado.DifferentialEquation(0.0, 1.0);
    f.add(dot(x1) == cos(x3));
    f.add(dot(x2) == sin(x3));
    f.add(dot(x3) == u);
    
    %% Optimal Control Problem (OCP)
    t0 = 0;
    tf = 1;
    N = 100;
    ocp = acado.OCP(t0, tf, N);
    
    %% Objective
    ocp.minimizeLagrangeTerm( (c1*1 + c2*(1+cos(2*pi*t)) + c3*(1+cos(2*pi*2*t)) + ...
                                c4*(1+sin(2*pi*t)) + c5*(1+sin(2*pi*2*t))) * u*u );
    %% Constraints
    ocp.subjectTo( f );
    ocp.subjectTo( 'AT_START', x1 == 0 );
    ocp.subjectTo( 'AT_START', x2 == 0 );
    ocp.subjectTo( 'AT_START', x3 == 0 );
    ocp.subjectTo( 'AT_END', x1 == xf1 );
    ocp.subjectTo( 'AT_END', x2 == xf2 );
    ocp.subjectTo( 'AT_END', x3 == xf3 );
    ocp.subjectTo( -50 <= u <= 50 );
    
    algo = acado.OptimizationAlgorithm(ocp);
    algo.set('DISCRETIZATION_TYPE','MULTIPLE_SHOOTING');
    algo.set('KKT_TOLERANCE',1e-9);
    algo.set('INTEGRATOR_TOLERANCE',1e-9);
    algo.set('ABSOLUTE_TOLERANCE',1e-9);
    
    %% Initial Guess
    %tsamp = linspace(0,1,50)';
    %xsamp = zeros(length(tsamp), 3);
    %xsamp(:,2) = linspace(0,1,50)';
    %M = [tsamp, xsamp];
    algo.initializeDifferentialStates(Mx);
    
    %usamp = zeros(length(tsamp), 1);
    %M = [tsamp, usamp];
    algo.initializeControls(Mu);
    
END_ACADO;

% tic
% c = [1.0, 0, 0, 0, 0]';
% xf = [0.1687, -0.6698, 0.4209]';
% out = elastica_RUN(c(1),c(2),c(3),c(4),c(5), xf(1), xf(2), xf(3));
% toc
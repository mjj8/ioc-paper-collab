function [ terminalCost, integrandCost, terminalJac, integrandJac ] = elasticagpopsPerturbCost( sol )

global c k epsilon k2

N = length(sol.time);
t = sol.time;    % N x 1 column vector
x = sol.state;   % N x n matrix, N number of collocation points, n is dimension of state
u = sol.control; % N x m matrix, N number of collocation points, m is dimension of control
p = sol.parameter;

n = length(x(1,:));
m = length(u(1,:));

terminalCost = 0; % terminal cost
terminalJac = [zeros(1,n), 0, zeros(1,n), 0, zeros(1,length(p))];
dL = zeros(N,n+m);
integrandCost = zeros(size(t));
for i = 1:N
    [phi_fourier, dphi_fourier] = phi_fourier_mult( t(i), x(i,:)', u(i,:)', k2 );
    integrandCost(i) = c' * phi_elastica(t(i),x(i,:)',u(i,:)',k) + epsilon' * phi_fourier;
    dL(i,:) = [0, 0, 0, 2*c'*sigma(t(i),k)*u(i,1)] + epsilon' * dphi_fourier;
end
integrandJac = [ dL, zeros(size(t)), zeros(length(t), length(p))];

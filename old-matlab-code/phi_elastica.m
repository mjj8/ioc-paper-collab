function [ vec ] = phi_elastica( t, x, u, k )
%
% t: N x 1
% x: 3 x N 
% u: 1 x N
% 


if strcmp(class(t),'ad')
    tsamp = t.value;
else
    tsamp = t;
end
if strcmp(class(x),'ad')
    xsamp = x.value(:,:);
elseif strcmp(class(x),'function_handle')
    xsamp = x(tsamp);
else
    xsamp = x;
end
if strcmp(class(u),'ad')
    usamp = u.value(:,:);
elseif strcmp(class(u),'function_handle')
    usamp = u(tsamp);
else
    usamp = u;
end

N = length(tsamp);
vec = zeros(k,N);

for i=1:N
    vec(:,i) = sigma(t(i), k) * usamp(i)^2;
end


end


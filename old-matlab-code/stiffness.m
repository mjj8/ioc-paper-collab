function [ output ] = stiffness( c, sigma, k )

%output = @(t) repmat(c',length(t),1) * sigma(t,k);
output = @(t) c' * sigma(t,k);
end


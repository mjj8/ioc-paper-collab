function val = sigma( t, k )


if strcmp(class(t),'ad')
    t = t.value;
end

N = length(t);

%% Sinusoidal basis functions
val = zeros(k,N);

for j = 1:N,
    val(1,j) = 1.0;
    
%     for i = 1:(k-1)/2,
%         val(i+1,j) = 1 + cos(2*pi*i*t(j));
%     end
%     for i = 1:(k-1)/2,
%         val(i+1+(k-1)/2,j) = 1 + sin(2*pi*i*t(j));
%     end

    for i = 2:k
        ii = i-1;
        if mod(ii,2) == 0  % even
            val(i,j) = 1 + sin(2*pi * round(ii/2) * t(j));
        else  % i odd
            val(i,j) = 1 + cos(2*pi * round(ii/2) * t(j));
        end
    end
    
end

%disp('t = ')
%disp(t)
%N = length(t);
% val = zeros(k,1);
% for i = 1:(k)
%     val(i) = t^(i-1);
% end

end


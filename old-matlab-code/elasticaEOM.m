function xdot = elasticaEOM(t, x, mu)

    xdot = zeros(3,1);

    xdot(1) = cos(x(3));
    xdot(2) = sin(x(3));
    xdot(3) = mu(t, x);

end

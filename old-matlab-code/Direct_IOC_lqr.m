function [ out ] = Direct_IOC_lqr(t0, tf, x0, xf, c, xopt, uopt, Vopt, A, B, timestamp)
% out = Direct_IOC_lqr(t0, tf, x0, xf, c, timestamp)

n = length(x0);
m = length(uopt(0));
k = length(c);  % number of cost basis functions

T = linspace(t0, tf, 50);
xuopt = [reshape(xopt(T)', n*length(T), 1) ; reshape(uopt(T)',m*length(T),1)]; % stack of all x's and u's
costopt = c' * Vopt;

%% Solve the inverse optimal control problem for unknown cost function parameters.
c0 = 0.1*ones(size(c));  % 
c0(1) = 0.1;            % initial guess for unknown parms

%% Some shared variables to store history of algorithm
i = 1;
chist = {};
xhist = {};
uhist = {};
feathist = {};
costhist = {};
objectivehist = {};

%options = optimset('MaxIter',200);
options = optimset('OutputFcn',@outputfunc,'TolFun',1e-3,'TolX',1e-3);
LB = 0.001*ones(length(c0)-1,1);
UB = 10*ones(length(c0)-1,1);
%% solve.
tic;
[chat,fval,exitflag, output] = fminsearchbnd(@(c_remainder)objective(c_remainder,t0,tf,x0,xf,xuopt),c0(2:end),LB,UB,options);
%[chat, fval, exitflag, output, fminlambda, grad, hessian] = fmincon( @(c)objective(c,t0,tf,x0,xf,xopt,uopt,k), c0, A, b, [],[],[],[],[],options)
totalTime = toc;


out.totaTime = totalTime;
out.t0 = t0;
out.tf = tf;
out.x0 = x0;
out.xf = xf;
out.c = c;
out.xopt = xopt;
out.uopt = uopt;
out.Vopt = Vopt;
out.costopt = costopt;
out.chat = chat;
out.whist = {};
out.xhist = xhist;
out.uhist = uhist;
out.feathist = feathist;
out.costhist = costhist;
out.chist = chist;
out.objectivehist = objectivehist;
out.fval = fval;
out.exitflag = exitflag;
out.fminoutput = output;


outdir = userpath;
outdir = outdir(1:end-1);
if timestamp,
    filename = [outdir filesep timestamp '-direct-lqr.mat'];
else
    filename = [outdir filesep datestr(now,'yyyymmdd-HHMMSS') '-direct-lqr.mat'];
end
save(filename, 'out');



    function [ output ] = objective( c_remainder, t0, tf, x0, xf, xuopt )
        %output = maxmargin_lqr2_objective( c )
        %
        %   c: variables of optimization = weights in forward problem cost function
        %
        
        ci = [0.1; c_remainder];
        
        [xi, ui, ~, ~, ~, ~] = lqrMain(t0, tf, x0, xf, ci, A, B);
        xu_predicted = [reshape(xi(T)', n*length(T), 1) ; reshape(ui(T)',m*length(T), 1)];
        feathist{i} = FeatureExpectations(xi, ui, t0, tf, @phi_quadratic);
        output = sum((xuopt - xu_predicted).^2);

        chist{i} = ci;
        xhist{i} = xi;
        uhist{i} = ui;
        costhist{i} = ci'*feathist{i};
        objectivehist{i} = output;
        try
            fprintf('i = %d\n',i);
            fprintf('delta c = %e\n',norm(chist{i} - chist{i-1}));
            fprintf('objective (traj err) = %e\n',output);
        catch
        end
        i = i + 1;
    end

    function stop = outputfunc(x, optimvalues, state)
        stop = false;
        
        switch state
            case 'iter'
                % check to see if we should stop.
                % is objective value less than epsilon?
                if (optimvalues.fval/norm(xuopt)^2) < 1e-5
                    stop = true;
                end
                % are the variables of optimization not changing enough?
                if length(chist) > 1
                    if norm(chist{end} - chist{end-1}) < 1e-4
                        stop = true;
                    end
                end
                % 
            case 'interrupt'
                % check to see if we should stop.
            otherwise
        end % end switch
    end

end
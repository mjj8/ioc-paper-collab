function [ dae, deriv ] = elasticagpopsDAE( sol )

t = sol.time; % N x 1 col vector
x = sol.state; % N x n matrix
u = sol.control; % N x m matrix
p = sol.parameter;

dae = [cos(x(:,3)) sin(x(:,3)) u(:,1)];

df1_dx1 = zeros(size(t));
df1_dx2 = zeros(size(t));
df1_dx3 = -sin(x(:,3));
df1_du1 = zeros(size(t));
df2_dx1 = zeros(size(t));
df2_dx2 = zeros(size(t));
df2_dx3 = cos(x(:,3));
df2_du1 = zeros(size(t));
df3_dx1 = zeros(size(t));
df3_dx2 = zeros(size(t));
df3_dx3 = zeros(size(t));
df3_du1 = ones(size(t));

df1_dp = zeros(length(t),length(p));
df1_dt = zeros(size(t));
df2_dp = zeros(length(t),length(p));
df2_dt = zeros(size(t));
df3_dp = zeros(length(t),length(p));
df3_dt = zeros(size(t));

deriv = [df1_dx1, df1_dx2, df1_dx3, df1_du1, df1_dt, df1_dp; ...
         df2_dx1, df2_dx2, df2_dx3, df2_du1, df2_dt, df2_dp; ...
         df3_dx1, df3_dx2, df3_dx3, df3_du1, df3_dt, df3_dp];

end


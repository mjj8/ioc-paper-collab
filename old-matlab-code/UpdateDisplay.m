function D = UpdateDisplay(conf,D)
% Define width of wire + length of caps --- must match collision detection
r = (0.75*(2^-5));
s = 5*r;

% Parameters for rendering
ntheta = 12;
nt = 40;

% Get curve
qsol = conf.qsol;
t = linspace(0,1,nt);
q = deval(qsol,t);

% Get vertices for rendering of wire
V = GetVerts(q,nt,ntheta,r,s);

% Either create display or just update it
if (nargin==2)
    
    set(D.wire,'vertices',V.wire');
    set(D.startgripper,'vertices',V.startgripper');
    set(D.startcap,'vertices',V.startcap');
    set(D.goalgripper,'vertices',V.goalgripper');
    set(D.goalcap,'vertices',V.goalcap');
    
elseif (nargin==1)
    
    % Define colors
    half_egrey=[0.6941,0.7804,0.9098];
    ered=[1,0.6,0];
    egrey=[0.4745,0.6471,0.9098];
    eblue=[0,0.4,1];
    ebrown=[0.4,0.2,0];
    yellow=[1,1,0];
    half_ered=[1,0.8,0.498];
    
    % Create new figure
    D.figure = figure('position',[150 -300 800 800],'color','w','renderer','opengl');
    D.axes = axes('position',[0 0 1 1],'clipping','off');

    % Create axis
    axis(1.25*[0 1 -.5 .5 -.5 .5]);
    axis equal;
    hold on;
    axis off

    % WIRE
    i = repmat(reshape(repmat(ntheta*((1:(nt-1))'-1),1,ntheta)',[],1),1,4);
    iplus = repmat([(1:ntheta)' (ntheta+(1:ntheta))' [(ntheta+(2:ntheta)) ntheta+1]' [2:ntheta 1]'],nt-1,1);
    faces = i+iplus;
    colors(:,:,1) = (239/255)*ones(1,size(faces,1));
    colors(:,:,2) = (138/255)*ones(1,size(faces,1));
    colors(:,:,3) = (28/255)*ones(1,size(faces,1));
    D.wire = patch('vertices',V.wire','faces',faces,'cdata',colors,'facecolor','flat','edgecolor',0.1*[1 1 1],'facelighting','phong');

    % GRIPPERS
    % start
    clear colors
    faces = [(1:ntheta)' (ntheta+(1:ntheta))' [(ntheta+(2:ntheta)) ntheta+1]' [2:ntheta 1]'];
    colors(:,:,1) = (110/255)*ones(1,size(faces,1));
    colors(:,:,2) = (139/255)*ones(1,size(faces,1));
    colors(:,:,3) = (191/255)*ones(1,size(faces,1));
    D.startgripper = patch('vertices',V.startgripper','faces',faces,'cdata',colors,'facecolor','flat','edgecolor',0.1*[1 1 1],'facelighting','phong');
    D.startcap = patch(V.startcap(1,:),V.startcap(2,:),V.startcap(3,:),half_egrey,'edgecolor',0.1*[1 1 1],'facelighting','phong');
    % goal
    D.goalgripper = patch('vertices',V.goalgripper','faces',faces,'cdata',colors,'facecolor','flat','edgecolor',0.1*[1 1 1],'facelighting','phong');
    D.goalcap = patch(V.goalcap(1,:),V.goalcap(2,:),V.goalcap(3,:),half_egrey,'edgecolor',0.1*[1 1 1],'facelighting','phong');

    % Tweak the lighting
    lighting flat
    light('Position',[0 -2 -1])
    light('Position',[0 -2 1])
    
    
    material shiny
    
    % Draw axes at the endpoint
    e1 = [1,0,0,1]';
    e2 = [0,1,0,1]';
    e3 = [0,0,1,1]';
    tmpscale = 0.1;
    ee = [e1,e2,e3];
    qend = reshape(q(:,end),4,4);
    eeplot = tmpscale * ee;
    eeplot(4,:) = 1;
    eenew = (qend*eeplot);
    line([0, eeplot(1,1)], [0, eeplot(2,1)], [0, eeplot(3,1)],'Color','b');
    line([0, eeplot(1,2)], [0, eeplot(2,2)], [0, eeplot(3,2)],'Color','g');
    line([0, eeplot(1,3)], [0, eeplot(2,3)], [0, eeplot(3,3)],'Color','r');
    line([qend(1,4), eenew(1,1)], [qend(2,4), eenew(2,1)], [qend(3,4), eenew(3,1)],'Color','b');
    line([qend(1,4), eenew(1,2)], [qend(2,4), eenew(2,2)], [qend(3,4), eenew(3,2)],'Color','g');
    line([qend(1,4), eenew(1,3)], [qend(2,4), eenew(2,3)], [qend(3,4), eenew(3,3)],'Color','r');
else
    
    error('wrong number of arguments in UpdateDisplay');
    
end

drawnow;




function V = GetVerts(q,nt,ntheta,r,s)
% WIRE
verts = [];
theta = linspace(0,2*pi,ntheta);
p1 = [zeros(1,ntheta); r*cos(theta); r*sin(theta)];
p1 = [p1; ones(1,size(p1,2))];
for i=1:nt
    T = reshape(q(:,i),4,4);
    p0 = T*p1;
    verts = [verts p0(1:3,:)];
end
V.wire = verts;
% GRIPPERS
% start
verts = [-s*ones(1,ntheta) zeros(1,ntheta)
          r*cos(theta) r*cos(theta)
          r*sin(theta) r*sin(theta)];
V.startgripper = verts;
verts = verts(:,1:ntheta);
V.startcap = verts;
% goal
p1 = [zeros(1,ntheta) s*ones(1,ntheta)
      r*cos(theta) r*cos(theta)
      r*sin(theta) r*sin(theta)];
p1 = [p1; ones(1,size(p1,2))];
p1 = T*p1;
verts = p1(1:3,:);
V.goalgripper = verts;
verts = verts(:,ntheta+1:end);
V.goalcap = verts;



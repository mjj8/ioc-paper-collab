function [ chist, whist, xhist, uhist, phist, feathist, costhist, forwardtimes ] = maxmargin_elastica_a(t0, tf, x0, xf, c0, xopt, uopt)
%   [chist, whist, feathist] = maxmargin(c0, D0, Dopt)
%
%   Inputs:
%       x0: initial state
%       xf: goal state
%       c0: initial guess for cost function parameters.
%       Dopt: the given optimal trajectory (i.e. the expert observation).
%
%   Return value:
%       chist: The history of iterates, c_i
%       whist: The history of margins w_i
%       feathist: history of feature expectations 
%

n = 3;
m = 1;
k = length(c0);

i = 1 % counter to keep track of number of iterations

% optimal trajectory (observation) feature expectations.
Vopt = FeatureExpectations(xopt, uopt, t0, tf, @phi_elastica, k);
%integrand = @(t) phi_elastica(t, xopt, uopt, k);
%Vopt = quadv(integrand, t0, tf);

% Solve forward problem for initial cost function guess.
% Initial guess for shooting method: straight line
tsamp = linspace(0,1,50)';
xsamp = zeros(length(tsamp), 3);
xsamp(:,2) = linspace(0,1,50)';
usamp = zeros(length(tsamp), 1);
Mx = [tsamp, xsamp];
Mu = [tsamp, usamp];
forwardstart = tic;
[x1, u1, p1, ~, ~, ~] = elasticaacadoMain(t0, tf, x0, xf, c0, Mx, Mu);
forwardtimes{i} = toc(forwardstart);
feathist{i} = FeatureExpectations(x1, u1, t0, tf, @phi_elastica, k);
disp('feathist(1) = ')
disp(feathist{i});
costhist{i} = c0' * feathist{i}
%integrand = @(t) phi_elastica(t, xopt, uopt, k);
%feathist{i} = quadv(integrand, t0, tf);


% Initialize some arrays that we will be using.
xhist{i} = x1; % optimal trajectory iterates
uhist{i} = u1; % optimal control iterates
phist{i} = p1; % optimal costate iterates



chist{i} = c0;
whist{i} = 1;
epsilon = 1e-3;  % threshold for terminating iterations.
%N = size(A,2);

% Linear inequality constrains: A c <= b
A = [];
b = [];

% Constrain the parameters to be positive: A2 c <= b
% Note that we constrain the control cost to be bounded away from zero 
% to avoid numerical issues with forward problem solver.
% This would all be handled more gracefully if we use unconstrained
% parameters, and then project them onto a desired parameter subspace
% (Ratliff et al.).
A2 = -eye(k); 
A2(k+1,k+1) = 0;
b2 = zeros(k+1,1);  


% Constrain the first cost function parameter to be equal to "1".  
% This is our standard normalization step.
Aeq = zeros(1,k+1);
Aeq(1) = 1.0;
beq = 1;

i = 2; % we've already initialized stuff
imax = 100;

% initial guess of unknown parms.  note that we append a guess of zero
% for the unknown "margin" for the fmincon optimization problem.
c0hat = [c0; 0]; 
options = optimset('Algorithm','interior-point','TolFun',1e-6,'TolCon',1e-6,'TolX',1e-6);
while(abs(whist{end}) > epsilon),
    % list of linear inequality constraints: A x <= b  where "x" is the
    % optimization variable, in our case "x" = c.
    A = [A, [Vopt - feathist{end}; -1]];  % append new linear inequality constraint
    b = zeros(i-1,1); 
    %N = size(A,2); % number of columns is the number of constraints we have

    % Solve quadratic program (SVM problem).
    %disp('size([A^T; A2]');
    %disp(size([A';A2]));
    %disp('size([b: b2])) = ');
    %disp(size([b; b2]));
    cstari = fmincon( @(c)c(end), c0hat, [A';A2], [b; b2], Aeq, beq, [], [], @maxmargin_nonlincon, options)
    % project cstari onto convex feasible region:
    %cstari = projection_elastica(cstari);
    
    whist{i} = cstari(end);
    chist{i} = cstari(1:end-1);

    % cut iterations off if convergence is taking too long
    if i > imax
        break;
    end
    
    % Solve forward problem given:  start and goal state, cost func parms c.
    % use previous result as initialization for current shooting method
    % iteration.
    xsamp = xhist{end}(tsamp);
    usamp = uhist{end}(tsamp);
    Mx = [tsamp, xsamp'];
    Mu = [tsamp, usamp];
    forwardstart = tic;
    [xi, ui, pi, ~, ~, ~] = elasticaacadoMain(t0, tf, x0, xf, chist{i}, Mx, Mu);
    forwardtimes{i} = toc(forwardstart);
    xhist{i} = xi;
    uhist{i} = ui;
    phist{i} = pi;
    feathist{i} = FeatureExpectations(xi,ui,t0,tf,@phi_elastica,k);
    costhist{i} = chist{i}' * feathist{i};

    i = i+1

end % end while loop


end


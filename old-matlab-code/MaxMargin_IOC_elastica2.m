function [ out ] = MaxMargin_IOC_elastica2(t0, tf, x0, xf, c, timestamp)
% out = MaxMargin_IOC()
%


%t0 = 0;
%tf = 1;
%x0 = [0, 0, 0]';
%xf = [0.4922, -0.5769, -1.8326]';
%xf = [0.1687, -0.6698, 0.4209]';
%xf = [-0.0555; -0.1816;1.3209];

% True cost function parameters, and associated optimal trajectory
%c = [1,0,0.5]';
k = length(c);  % number of cost basis functions

[xopt, uopt, popt, xoptp, uoptp, poptp] = elasticagpopsMain(t0, tf, x0, xf, c);
Vopt = FeatureExpectations(xopt, uopt, t0, tf, @phi_elastica, k);


%% Solve the inverse optimal control problem for unknown cost function parameters.
c0 = zeros(size(c));  % 
c0(1) = 1;            % initial guess for unknown parms

%% Some shared variables to store history of algorithm
i = 1;
chist = {};
xhist = {};
uhist = {}
feathist = {};
costhist = {};

%% inequality constraints:  enforce positivity 
A = -eye(k); 
b = zeros(k,1);
%% equality constraints: enforce c(1) == 1 as our normalization approach.
%Aeq = zeros(1,k);
%Aeq(1) = 1.0;
%beq = 1;
%% objective function
%objective = @(c)maxmargin_elastica2_objective(c,t0,tf,x0,xf,Vopt,k);
options = optimset('Algorithm','interior-point','MaxIter',200);
%% solve.
tic;
[chat, fval, exitflag, output, fminlambda, grad, hessian] = fmincon( @(c)objective(c,t0,tf,x0,xf,Vopt,k), c0, A, b, [],[],[],[],[],options)
totalTime = toc;

out.totaTime = totalTime;
out.t0 = t0;
out.tf = tf;
out.x0 = x0;
out.xf = xf;
out.c = c;
out.xopt = xopt;
out.uopt = uopt;
out.Vopt = Vopt;
out.chat = chat;
out.whist = {};
out.xhist = xhist;
out.uhist = uhist;
out.feathist = feathist;
out.cost = costhist;
out.chist = chist;
out.fval = fval;
out.exitflag = exitflag;
out.fminoutput = output;
out.lambda = fminlambda;
out.grad = grad;
out.hessian = hessian;


outdir = userpath;
outdir = outdir(1:end-1);
if timestamp,
    filename = [outdir filesep timestamp '-max-margin-elastica2.mat'];
else
    filename = [outdir filesep datestr(now,'yyyymmdd-HHMMSS') '-max-margin-elastica2.mat'];
end
save(filename, 'out');



    function [ output ] = objective( c, t0, tf, x0, xf, Vopt, k )
        %output = maxmargin_elastica2_objective( c )
        %
        %   c: variables of optimization = weights in forward problem cost function
        %
        
        c
        
        lambda = 1;
        
        %Vopt = FeatureExpectations(xopt, uopt, t0, tf, @phi_elastica, k);
        cost_opt = c' * Vopt;
        
        % min c' * FeatureExpectation(mu)   (or loss-function variant)
        [x, u, ~, ~, ~, ~] = elasticagpopsMain(t0, tf, x0, xf, c);
        V = FeatureExpectations(x,u,t0,tf,@phi_elastica,k);
        cost = c' * V;
        
        chist{i} = c;
        xhist{i} = x;
        uhist{i} = u;
        feathist{i} = V;
        
        output = cost_opt - cost + (lambda/2)*norm(c)^2
        
        costhist{i} = output;
        i = i + 1;
    end

end






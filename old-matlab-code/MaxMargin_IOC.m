function [ out ] = MaxMargin_IOC( )
% out = MaxMargin_IOC()
%

x0 = [0, 0, 0]';
xf = [0.4922, -0.5769, -1.8326]';
%xf = [0.1687, -0.6698, 0.4209]';
%xf = [-0.0555; -0.1816;1.3209];

% True cost function parameters, and associated optimal trajectory
ctrue = [1,0]';

p0guess = [60,-60,6];
[xguess, uguess, pguess] = getElasticaGivenP0(x0, p0guess, ctrue);
[xsamp, xopt, uopt, popt, xprime,uprime,pprime] = Forward_Problem(x0, xf, ctrue, xguess, uguess);
[x2,u2,p2] = getElasticaGivenP0(x0, popt(0), ctrue);
%[x3,u3,p3] = getElasticaGivenP0(x0, [60,-60,6]', ctrue);

disp('popt(t=0) = ')
disp(popt(0))
disp('x2(t=1) = ')
disp(x2(1))

%% Compute the arc length of the trajectory:
subindex = @(v,i,j) v(i:j,:);
tmpfun = @(t) sqrt(dot(subindex(xprime(t),1,2),subindex(xprime(t),1,2),1));
%tmpfun2 = @(t) sqrt(dot(subindex(xprime2(t),1,2),subindex(xprime2(t),1,2),1));
arclen = quad(tmpfun, 0, 1);
%arclen2 = quad(tmpfun2,0,1);
disp('arclen(xopt) = ');
disp(arclen);


%% Compute the cost of the trajectory:
%cost1 = @(t) ctrue'*psi(t,uopt(t));
%out = quad(myfun, t0, tf);

plotElastica(xopt,x2);
plotElasticaCostate(popt, p2);

%% Solve the inverse optimal control problem for unknown cost function parameters.
%c0 = [1,0]';
%c = maxmargin( x0, xf, c0, Dopt);

%out = c;
out =0
end


function [xopt, uopt, popt, xoptp, uoptp, poptp] = lqrMain(t0, tf, x0, xf, c, A, B)


n = length(x0);
m = size(B,2);

% convert c to Q and R matrices
Q = diag(c(1:n));        %Q = diag(c(1:3));
R = diag(c(n+1:n+m));    %R = diag(c(4:5));

% Riccati solution
P0 = zeros(n*n,1);
options = odeset('RelTol',1e-8,'AbsTol',1e-8);
sol = ode45(@(t,x)rde(t, x, A, B, Q, R), [tf, t0], P0, options);
P = @(t)deval(sol,t);

% Simulate system under optimal control
solx = ode45(@(t,x)f(t,x,A,B,R,P), [t0, tf], x0, options);
xopt = @(t) deval(solx,t);
uopt = @(t) lqrMu(t,xopt,B,R,P);
popt = nan;
xoptp = nan;
uoptp = nan;
poptp = nan;


end % end function lqrMain

function xdot = f(t,x,A,B,R,P)

n = size(A,1);
m = size(B,2);

% closed loop control:
Pt = reshape(P(t),n,n);
K = inv(R)*B'*Pt;
u = -K*x;

xdot = A*x + B*u;

end

function pdot = rde(t,x,A,B,Q,R)
% Riccati Differential Equation

n = size(A,1);
m = size(B,2);
%n = 3;
%m = 2;

P = reshape(x,n,n);

pdot = -A'*P - P*A + P*B*inv(R)*B'*P - Q;
pdot = reshape(pdot, n*n, 1);

end
function [ u ] = lqrMu( t, x, B, R, P )

N = length(t);
n = length(x(0));
m = size(R,1);
u = zeros(m,N);

Pt = reshape(P(t),n,n,N);

for i = 1:N,
    u(:,i) = -inv(R)*B'*Pt(:,:,i)*x(t(i));
end

end


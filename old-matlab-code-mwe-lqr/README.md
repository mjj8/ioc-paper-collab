README
======

To run this minimum working example:

```
 $ cd ioc-paper-collab/old-matlab-code-mwe-lqr
 $ matlab
 >> run_mwe
```

Some output will be printed to std out.  Output data files will be written to
*userpath*, which on MAC OS X defaults to ~/Documents/MATLAB/.

The general pattern of this old code is:
 1. Define the dynamic system, initial conditions, and time horizon.
 2. Define cost function basis functions `(phi_i(t) : (x(t), u(t)) -> R_+)`
 3. Define a random weight vector, c, for the cost function.
 4. Compute an optimal trajectory assuming c is known. (if the system does not
    have an analytical solution, use a nonlinear optimization package such as
    GPOPS or ACADO)
 5. Assume c is not known, use IOC method to learn c.
 6. Compare learned weight vector, c_hat, with the true value, c.  (also
    compare predicted and true feature expectations, total cost, etc.)


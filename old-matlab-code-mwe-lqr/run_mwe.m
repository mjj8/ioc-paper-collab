clear global variables

global c xf;
    
curFolder = pwd;
addpath(curFolder);

% GENERATE TIMESTAMP FOR OUTPUT FILE
timestamp = datestr(now,'yyyymmdd-HHMMSS');

% DEFINE LINEAR SYSTEM, INITIAL CONDITION, AND TIME HORIZON
t0 = 0;
tf = 10;
n = 3;
m = 2;
A = randn(n,n);
[v,e] = eig(A);
maxe = max(abs(diag(e)));
A = A/maxe
B = randn(n,m)
x0 = 5*randn(3,1)
xf = nan;

% SELECT A RANDOM WEIGHT VECTOR FOR THE COST FUNCTION
c = [0.1; 10.^(-2*rand(n+m-1,1))]

% COMPUTE THE OPTIMAL TRAJECTORY GIVEN KNOWN COST FUNCTION
[xopt, uopt, popt, xoptp, uoptp, poptp] = lqrMain(t0, tf, x0, xf, c, A, B);
% COMPUTE FEATURE EXPECTATIONS OF OPTIMAL TRAJECTORY FOR LATER COMPARISON
Vopt = FeatureExpectations(xopt,uopt,t0,tf,@phi_quadratic)
% GATHER OPTIMAL TRAJECTORY FOR LATER COMPARISON
x0list{1} = x0;
xflist{1} = xf;
xoptlist{1} = xopt;
uoptlist{1} = uopt;
Voptlist{1} = Vopt;

% INVERSE OPTIMAL CONTROL -- LEARN VALUE OF c, SAVE RESULTS TO FILE
% out = Direct_IOC_lqr(t0, tf, x0, xf, c, xopt, uopt, Vopt, A, B, timestamp);
% out1 = MaxMargin_IOC_lqr(t0, tf, x0, xf, c, xopt, uopt, Vopt, A, B, timestamp);
% out3 = MaxMargin_IOC_lqr3(t0, tf, x0, xf, c, xopt, uopt, Vopt, A, B, timestamp);
out4 = KKT_IOC_lqr( t0, tf, x0list, xflist, c, xoptlist, uoptlist, Voptlist, A, B, timestamp);

function [ out ] = FeatureExpectations( x, u, t0, tf, phi, varargin )
% out = FeatureExpectations( D )
%
%   Inputs:
%       x, u: state and input trajectory, x(t), u(t).
%       phi: cost basis function, phi(t, x, u, args)   *Needs to be vectorizable*.
%       varargin: any further parameters that should be passed to phi().
%
%   Return value:
%       out: a vector of dimension "k" -- number of objective basis
%       functions)
%

tic
%integrand = @(t) vertcat(x(t).^2, u(t).^2); %dot(x(t), Q*x(t)) + dot(u(t), R*u(t));
integrand = @(t) phi(t, x, u, varargin{:});
out = quadv(integrand, t0, tf, 1e-6);

time = toc;
%fprintf('Time to compute feature vector = %e\n',time);

end

